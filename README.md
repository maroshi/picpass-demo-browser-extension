# Picpass demo, browser extension

Password entry without keybord, using pictures selection.

## Installation
1. Install `node.js` into your operating system from https://nodejs.org/en/download/
2. Install `git` into your operating system from https://git-scm.com/downloads 
3. Install application repository: In a new empty directory run command line `git clone https://maroshi@bitbucket.org/maroshi/picpass-demo-browser-extension.git`
4. Build development repository: Change directory into `picpass-demo-browser-extension` run command line `npm install` .

## Further help
See Picpass soulution site documentation http://homeil.org/documentation
Or contact us from http://homeil.org/contact-us/

Please report problems/bugs in https://bitbucket.org/maroshi/picpass-demo-browser-extension/issues
## Usage

Run `$ gulp --watch` and load the `dist`-directory into chrome.

## Entryfiles (bundles)

There are two kinds of entryfiles that create bundles.

1. All ts-files in the root of the `./app/scripts` directory
2. All css-,scss- and less-files in the root of the `./app/styles` directory

## Tasks

### Build

    $ gulp


| Option         | Description                                                                                                                                           |
|----------------|-------------------------------------------------------------------------------------------------------------------------------------------------------|
| `--watch`      | Starts a livereload server and watches all assets. <br>To reload the extension on change include `livereload.js` in your bundle.                      |
| `--production` | Minifies all assets                                                                                                                                   |
| `--verbose`    | Log additional data to the console.                                                                                                                   |
| `--vendor`     | Compile the extension for different vendors (chrome, firefox, opera, edge)  Default: chrome                                                                 |
| `--sourcemaps` | Force the creation of sourcemaps. Default: !production                                                                                                |


### pack

Zips your `dist` directory and saves it in the `packages` directory.

    $ gulp pack --vendor=firefox

### Version

Increments version number of `manifest.json` and `package.json`,
commits the change to git and adds a git tag.


    $ gulp patch      // => 0.0.X

or

    $ gulp feature    // => 0.X.0

or

    $ gulp release    // => X.0.0


## Globals

The build tool also defines a variable named `process.env.NODE_ENV` in your scripts. It will be set to `development` unless you use the `--production` option.


**Example:** `./app/background.ts`

```typescript
if(process.env.NODE_ENV === 'development'){
  console.log('We are in development mode!');
}
```
