/*
Copyright (c) 2021, Picpass solutions
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the Picpass solutions nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Picpass solutions BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
export class PicpassWidget {
  private static _picpassConfig: Object = {};
  private static _instance: PicpassWidget = <PicpassWidget>null;
  private static _modalObject: any;
  private static _initialPassword: string;
  private static _childWindow: Window;
  private static _dialogElementId: string = 'pp_main_dialog';

  public static getInstance() {
    if (PicpassWidget._instance == null) {
      PicpassWidget._instance = new PicpassWidget();
    }
    return PicpassWidget._instance;
  }

  public static sendMessage(text: string) {
    PicpassWidget._childWindow.postMessage({
      "topic": "apply-password-text",
      "message": text
    }, "*");
  }

  public static async open() {
      await PicpassWidget.getInstance();
      PicpassWidget.openDialog();
  }

  public static sendConfigRepo(data: object) {
    PicpassWidget._picpassConfig = data;
    const passwordElement: HTMLInputElement = PasswordElementDetector.passwordElement;
    PicpassWidget._initialPassword = passwordElement.value;
    PicpassWidget._picpassConfig['Initiation']['PasswordInitalValue'] = passwordElement.value;
    PicpassWidget._childWindow.postMessage({
        "topic": "register",
        "message": PicpassWidget._picpassConfig
    }, "*");
  }

  private constructor() {
    let bodyElemnt: HTMLBodyElement = document.querySelector('body');

    let oldPicpassDialogElement: HTMLDialogElement  = document.querySelector(`#${PicpassWidget._dialogElementId}`);
    // remove previous dialog, prevent dialog elements accumulation
    if (oldPicpassDialogElement != null) oldPicpassDialogElement.remove();

    // compute Picpass GUI url from config.
    const picpassUrl = this._computePicpassUrl();
    // compute Picpass GUI width and height
    const currPicpassWidth: number = this._computePicpassWidth();
    const currPicpassHeight: number = this._computePicpassHeight();
    // assign current password value to config

    // generate last body element with id = picpassDialogElementId
    let parentUrl: string = document.location.href;
    let picpassHtml: string = `
<dialog id="${PicpassWidget._dialogElementId}">
  <iframe id="${PicpassWidget._dialogElementId}_angular_app" src="${picpassUrl}" data-parent-url="${parentUrl}" name="picpass"
    width="${currPicpassWidth}" height="${currPicpassHeight}">
  </iframe>
</dialog>`;
    bodyElemnt.insertAdjacentHTML('beforeend', picpassHtml);

    // assign childWindow (used for message communication) to generated iFrame
    const iframeElement:  HTMLIFrameElement = document.querySelector(`#${PicpassWidget._dialogElementId}_angular_app`);
    PicpassWidget._childWindow = iframeElement.contentWindow;
    // PicpassWidget._childWindow.location.assign(picpassUrl);

    // open dialog box and register its close events
    let currPicpassDialogElement: HTMLDialogElement = document.querySelector(`#${PicpassWidget._dialogElementId}`);
    currPicpassDialogElement.addEventListener('close', function onClose(){
      console.log(`Closed Modal dialog id=${PicpassWidget._dialogElementId} at ${new Date().toISOString()}`);
    });
    let angularAppIframeElm: HTMLIFrameElement = document.querySelector(`#${PicpassWidget._dialogElementId}_angular_app`);
    let angularAppWin = angularAppIframeElm.contentWindow;

    window.addEventListener('message', (event: any) => {
          console.log(`parent received message!:  '${event.data.topic}'`);
          let messageTopic: string = event.data.topic;
          switch (messageTopic) {
              case 'submit-password':
                  PicpassWidget.submit();
                  break;
              case 'register':
                  PicpassWidget._childWindow = event.source;
                  PicpassWidget.sendConfigRepo(PasswordElementDetector.config);
                  break;
              case 'dialog-close':
                // console.log(`handling dialog-close message`);
                PicpassWidget.closeDialog();
                break;
              case 'apply-password-text':
                // console.log(`handling apply-password-text message`);
                PicpassWidget.applyPasswordText(<string>event.data.message);
                break;
              default:
                break;
          }
      });

      const passwordElement: HTMLInputElement = PasswordElementDetector.passwordElement;
      PicpassWidget._initialPassword = passwordElement.value;
  }
  public static submit() {
    const formElem: HTMLFormElement = PasswordElementDetector.passwordElement.form;
    formElem.submit();
  }

  private _computePicpassUrl() {
    // PasswordElementDetector.config can be wiped from borwser storage
    // if not exist read config from config.json
    if (typeof PasswordElementDetector.config == 'undefined' || PasswordElementDetector.config == null) {
      PasswordElementDetector.config = require('../config.json');
      chrome.storage.local.set({ 'picpass_config': PasswordElementDetector.config });
      console.warn(`!!! Reread config.json at ${new Date().toISOString()}`);
    }
    const connectionConfig: object = PasswordElementDetector.config['Communication'];
    const picpassConfig: object = connectionConfig['User interface'];
    const picpassUrl = `${picpassConfig['Protocol']}://${picpassConfig['Host']}:${picpassConfig['Port']}/${picpassConfig['Root context']}`;
    console.log(`picpassUrl=${picpassUrl}`);
    return picpassUrl;
  }

  private _computePicpassWidth(): number {
    //const vh = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0)
    const maxWindowsWidth: number = document.documentElement.clientWidth - 4;
    const initiationConfig: object = PasswordElementDetector.config['Initiation'];
    const maxPicpassWidth: number = initiationConfig['maxWidth'][1];
    return Math.min(maxPicpassWidth, maxWindowsWidth);
  }

  private _computePicpassHeight(): number {
    const initiationConfig: object = PasswordElementDetector.config['Initiation'];
    const maxWindowHeight: number = document.documentElement.clientHeight - 4;
    const maxPicpassHeight: number = initiationConfig['maxHeight'][1];
    return Math.min(maxWindowHeight, maxPicpassHeight);
  }

  public static openDialog() {
    const currPicpassDialogElement: HTMLDialogElement = document.querySelector(`#${PicpassWidget._dialogElementId}`);
    currPicpassDialogElement.showModal();
    PicpassWidget.sendMessage('opened dialog');
  }

  public static applyPasswordText(inText: string) {
    const passwordElement: HTMLInputElement = PasswordElementDetector.passwordElement;
    console.dir(passwordElement);
    passwordElement.value = inText;
      PicpassWidget._childWindow.postMessage({
          "topic": "apply-password-text",
          "message": {}
      }, "*");
      PicpassWidget.sendMessage('apply password text');
  }

  public static closeDialog() {
      const currPicpassDialogElement: HTMLDialogElement = document.querySelector(`#${PicpassWidget._dialogElementId}`);
      currPicpassDialogElement.close();
      PicpassWidget.sendMessage('close dialog');

      const passwordElement: HTMLInputElement = PasswordElementDetector.passwordElement;
      // passwordElement.focus();
  }

  private static _resetPassward() {
    const passwordElement: HTMLInputElement = PasswordElementDetector.passwordElement;
    passwordElement.value = PicpassWidget._initialPassword;
  }
}
export class PasswordElementDetector {
  private _mutationsCount: number = 0;
  private static _instance: PasswordElementDetector = null;
  private static _passwordElement: HTMLInputElement = null;
  public static config: object = null;

  public static getInstance= ():PasswordElementDetector => {
    if (PasswordElementDetector._instance == null)
      PasswordElementDetector._instance = new PasswordElementDetector();
    return PasswordElementDetector._instance;
  }

  public static foundPasswordElement(): Boolean {
    return (PasswordElementDetector._passwordElement != null);
  }

  public static get passwordElement(): HTMLInputElement {
    return PasswordElementDetector._passwordElement;
  }

  public static sendMessage(text) {
    chrome.runtime.sendMessage({ passwordElement: text }, function (response) {
      console.log(response.acknoledgded);
    });
  }

  public static detect() {
    let currPrivateInstance = PasswordElementDetector.getInstance();
    PasswordElementDetector._passwordElement = null;
    currPrivateInstance._detect();
    if (PasswordElementDetector.foundPasswordElement) {
      PasswordElementDetector.sendMessage('found');
    } else {
      PasswordElementDetector.sendMessage('missing');
    }
  }
  // Observer for password element visibility. Inspecting: CSS properties and size
  // Using MutationObserver mechanism to inspect on each page mutation
  private _inpsectMutations = (mutations: MutationRecord[]) => {
    let mutation: MutationRecord;
    for (mutation of mutations) {
      let currMutationa = mutation;
      let currMutationaTarget = mutation.target;
      // console.dir(currMutationa);
      // console.dir(currMutationaTarget);
      this._mutationsCount;
      let passwordElements = document.querySelectorAll("[type=password]");
      for (let i = 0; i < passwordElements.length; ++i) {
        let passwordElement = passwordElements[i];
        if (this._isVisibleElement(passwordElement) === true) {
          // password is visible from CSS and dimmensions
          // stop inspecting page mutations
          this._mutationObserver.disconnect();
          // observe the password element rendering, detect when password element is rendered
          this._renderingObserver.observe(passwordElement);
        }
      }
    }
  }
  private _mutationObserver = new MutationObserver(this._inpsectMutations);

  // Inspect the password element rendering, detect when password element is mostly rendered
  // Using IntersectionObserver mechanism
  private _inspectRendering = (entries: IntersectionObserverEntry[]) => {
    // isIntersecting is true when element and viewport are overlapping
    // isIntersecting is false when element and viewport don't overlap
    let elm = entries[0];
    if(elm.isIntersecting === true)
      console.log(`Element ${elm.target} has just rendered visible on screen ${new Date().toISOString()} after ${this._mutationsCount} page mutataions.`);
      console.dir(elm.target);
      PasswordElementDetector._passwordElement = <HTMLInputElement>elm.target;
      // Password element is rendered and visible, stop inspecting
      this._renderingObserver.unobserve;
      // Just in case there is still vivisiblity inspection, stop it
      this._mutationObserver.disconnect();
      // notify background script that a password element identified
      PasswordElementDetector.sendMessage('found');
      PasswordElementDetector._registerPicpassWidget();
  }
  private _renderingObserver = new IntersectionObserver(this._inspectRendering, {
    root: document.querySelector('body'),
    threshold: [1.0]
  });

  private _detect() {
    // Initially find any password element in the page.
    let passwordElements = document.querySelectorAll('[type=password]');
    // console.log(`passwordElements = ${passwordElements.length}    mutationsCount = ${this._mutationsCount}`);
    // For each detected password element, inpsect its visibility
    passwordElements.forEach(passwordElement => {
      if (this._isVisibleElement(passwordElement) === true) {
        // Password element is visible, now observe it. Until it is fully rendered on screen as well
        // console.log(`Visible ${passwordElement} element at ${new Date().toISOString()}`);
        // console.dir(passwordElement);
        PasswordElementDetector._passwordElement = <HTMLInputElement>passwordElement;
        this._renderingObserver.observe(passwordElement);
      }
    });

    // Inspect any change on the current page (assuming it is SPA) to detect visible password element
    this._mutationObserver.observe(document.documentElement, {
      childList: true,
      subtree: true
    });
  }

  private constructor() {
    chrome.storage.local.get(['picpass_config'], (result) => {
      PasswordElementDetector.config = result['picpass_config'];
      console.log(`PasswordElementDetector constructed`);
      this._detect();
    });
  }

  // Detect Elements has minimal dimmension and is not hidden and is displayable
  private _isVisibleElement(el) {
    if (el instanceof Element) {
      let style = window.getComputedStyle(el);
      let parentStyle = window.getComputedStyle(el.parentElement);
      let width = el.getBoundingClientRect().width;
      let height = el.getBoundingClientRect().height;
      return (style.display !== 'none'
        && parentStyle.overflow !== 'hidden'
        && width > 20
        && height > 8
        && style.visibility !== 'hidden'
      )
    }
    return false;
  }

  private static _registerPicpassWidget() {
    PicpassWidget.getInstance();
    PasswordElementDetector._passwordElement.addEventListener('dblclick', (ev) => {
      console.log(`Double click detected in Passwrod Element at ${new Date().toISOString()}`);
      PicpassWidget.open();
    });
  }
//
  private static picpassMessageHandler(msg: object) {
    console.dir(msg);
    const currPicpassDialogElement: HTMLDialogElement = document.querySelector(`#pp_main_dialog`);
    switch(msg['topic']) {
      case 'submit-password':
        currPicpassDialogElement.close();
        break;
      case 'dialog-close':
        currPicpassDialogElement.close();
        break;
      case 'apply-password-text':
        PasswordElementDetector.passwordElement.value = msg['message'];
        break;
      default :
        console.log(`Message recieved at ${new Date().toISOString()} not handled!`);
        console.dir(msg);
      }
  }
}

// Start content page loading
console.log(`0100: ${new Date().toISOString()} Started contentPage.ts on page: ${window.location.href}`);

// Notify background script that a password element is missing by default
PasswordElementDetector.sendMessage('missing');

// communicate with background page, detected password element, that is visible and rendered
PasswordElementDetector.getInstance();

// Completed content page loading
console.log(`9999: ${new Date().toISOString()} Consumed contentPage.ts on page: ${window.location.href}`);

// Listen to detection request from background script
chrome.runtime.onMessage.addListener(
  function (request, sender, sendResponse) {
      // console.log(`received message on ${window.location.href}`)
      if (request.passwordElement === 'detect') {
        console.log(`received detect message on ${window.location.href}`);
        PasswordElementDetector.detect();
      } else if (request.passwordElement === 'isFound') {
        console.log(`received isFound message on ${window.location.href}`);
        sendResponse({ acknoledgded: `${PasswordElementDetector.foundPasswordElement()}` });
      }
  });
