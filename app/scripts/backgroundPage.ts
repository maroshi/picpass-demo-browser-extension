/*
Copyright (c) 2021, Picpass solutions
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the Picpass solutions nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Picpass solutions BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
console.log('background script loaded');

// By default icon is set to diesabled
function enableIcon()  {chrome.browserAction.setIcon({path: 'images/extension-icon-enabled.png'});}
function disableIcon() {chrome.browserAction.setIcon({path: 'images/extension-icon-disabled.png'});}
async function loadConfig() {
  const configObj: object = await require('../config.json');
  await chrome.storage.local.set({'picpass_config': configObj});
}

loadConfig();
disableIcon();

chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        // console.log(sender.tab ?
            // `Recieved message from a content script: ${sender.tab.url} ` : '');
        if (request.passwordElement === 'found') {
            enableIcon();
            // console.log('extension icon enabled');
            sendResponse({ acknoledgded: `Acknowledged "found password element" message at ${new Date().toISOString()}` });
        }
        else if (request.passwordElement === 'missing') {
            disableIcon();
            // console.log('extension icon disabled');
            sendResponse({ acknoledgded: `Acknowledged "missing password element" message at ${new Date().toISOString()}` });
        }
        else if (request.passwordElement === 'display') {
            // console.log('recieved request to display dialog');
            sendResponse({ acknoledgded: `Acknowledged "display dialog" message at ${new Date().toISOString()}` });
        }
    });

// On click send a password element detection reqeust to active page
/*
chrome.browserAction.onClicked.addListener(
    function () {
        // console.log(`Extension clicked at ${new Date().toISOString()}`);
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
            chrome.tabs.reload(tabs[0].id);
            // chrome.tabs.sendMessage(tabs[0].id, {passwordElement: "detect"});
            // console.log(`sent password element detection request at ${new Date().toISOString()}`);
          });
    });
    */

// On tab change inspect the target page for detected password element
chrome.tabs.onActivated.addListener(function (activeTabInfo) {
    let activeTabId: number = activeTabInfo.tabId;
    chrome.tabs.sendMessage(activeTabId, { passwordElement: "isFound" }, function (response) {
        if (!window.chrome.runtime.lastError) {
            console.log(response.acknoledgded);
            if (response.acknoledgded === "true")
                enableIcon()
            else
                disableIcon();
        } else { disableIcon(); }
    });
});
