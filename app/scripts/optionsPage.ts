/*
Copyright (c) 2021, Picpass solutions
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the Picpass solutions nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Picpass solutions BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
// Enable chromereload by uncommenting this line:
// import 'chromereload/devonly'

export class OptionsPage {
  static config: object = null;
  static initObj: object = null;
  static featuresObj: object = null;
  static dataSourceUrl: string = null;
  static keywordsArr: string[] = [];

  constructor() {
    OptionsPage.initOptionsConfig();
    const resetButton: HTMLButtonElement = document.querySelector(`#resetButton`);
    resetButton.addEventListener('click', OptionsPage.onConfigReset);
  }

  static initKeywordsConfig() {
    fetch(`${OptionsPage.dataSourceUrl}/keywordPicsCounts/search/all`).then((response) => {
      return response.json();
    }).then((jsonObj) => {
      const keywordPicsCountsArr: object[] = jsonObj['_embedded']['keywordPicsCounts'];
      keywordPicsCountsArr.filter((keywordPicsCountsObj) => keywordPicsCountsObj['picsCount'] > 6)
                          .forEach((keywordPicsCountsObj) => OptionsPage.keywordsArr.push(keywordPicsCountsObj['name']));

      const configKeywordsArr: string[] = OptionsPage.initObj['Keywords:'];
      for (let i = 0; i < configKeywordsArr.length; i++) {
        const detailsElm: HTMLDetailsElement = document.querySelector('#keywords-list');
        let keywordContainerDiv: HTMLDivElement = document.createElement('div');
        keywordContainerDiv.className = 'form-group row px-1 m-0 mb-1';
        detailsElm.appendChild(keywordContainerDiv);

        let labelElm: HTMLLabelElement = document.createElement('label');
        labelElm.className = 'col-5 col-form-label text-truncate';
        labelElm.textContent = `secret keyword #${i + 1}`;
        keywordContainerDiv.appendChild(labelElm);

        let selectElm: HTMLSelectElement = document.createElement('select');
        selectElm.className = 'custom-select col-7 px-2';
        selectElm.id = i.toString();
        keywordContainerDiv.appendChild(selectElm);

        OptionsPage.keywordsArr.forEach((keyword) => {
          let optionElm: HTMLOptionElement = document.createElement('option');
          optionElm.value = keyword;
          optionElm.text = keyword;
          selectElm.appendChild(optionElm);
        });

        selectElm.value = configKeywordsArr[i];
        selectElm.addEventListener('input', (ev: Event) => {
          const target: HTMLSelectElement = ev.target as HTMLSelectElement;
          OptionsPage.initObj['Keywords:'][target.id] = target.value;
          chrome.storage.local.set({'picpass_config': OptionsPage.config });
        });
      }
    });
  }

  static initInputElement(elemId: string, eventHandler: any) {
    const divElm: HTMLElement = document.querySelector(`#${elemId}`);
    let labelElm: HTMLLabelElement = divElm.querySelector('label');
    let inputElm: HTMLInputElement = divElm.querySelector('input');
    inputElm.addEventListener('input', eventHandler);

    if (elemId === 'Password') {
      labelElm.innerText = elemId;
      inputElm.value = OptionsPage.initObj[elemId];
    } else if (elemId === 'progrebarPostions') {
      labelElm.innerText = OptionsPage.featuresObj[elemId][0];
      inputElm.value = OptionsPage.featuresObj[elemId][1];
    } else {
      labelElm.innerText = OptionsPage.initObj[elemId][0] + ':';
      inputElm.value = OptionsPage.initObj[elemId][1];
    }
  }

  static async initOptionsConfig() {
    await chrome.storage.local.get(['picpass_config'], (result) => {
      OptionsPage.config = result['picpass_config'];
      OptionsPage.initObj = OptionsPage.config['Initiation'];
      OptionsPage.featuresObj = OptionsPage.config['Features'];

      const dataSourceConfigObj: object = OptionsPage.config['Communication']['Data source'];
      OptionsPage.dataSourceUrl = `${dataSourceConfigObj['Protocol']}://${dataSourceConfigObj['Host']}:${dataSourceConfigObj['Port']}/${dataSourceConfigObj['Root context']}`;
      OptionsPage.initKeywordsConfig();

      OptionsPage.initInputElement('Password', OptionsPage.onTextInput);

      OptionsPage.initInputElement('maxSelections', OptionsPage.onNumberInput);
      OptionsPage.initInputElement('rows', OptionsPage.onNumberInput);
      OptionsPage.initInputElement('cols', OptionsPage.onNumberInput);
      OptionsPage.initInputElement('maxWidth', OptionsPage.onNumberInput);
      OptionsPage.initInputElement('maxHeight', OptionsPage.onNumberInput);
      OptionsPage.initInputElement('layoutSelections', OptionsPage.onNumberInput);
      OptionsPage.initInputElement('frameKeywords', OptionsPage.onNumberInput);

      OptionsPage.initCechboxElement('quitOnCompletion', OptionsPage.onCheckboxInput);
      OptionsPage.initCechboxElement('refreshButton', OptionsPage.onCheckboxInput);
      OptionsPage.initCechboxElement('quitButton', OptionsPage.onCheckboxInput);
      OptionsPage.initCechboxElement('keywordsRevealHint', OptionsPage.onCheckboxInput);
      OptionsPage.initCechboxElement('passwordConfiguration', OptionsPage.onCheckboxInput);
      OptionsPage.initCechboxElement('submitPassword', OptionsPage.onCheckboxInput);
      OptionsPage.initCechboxElement('resetPassword', OptionsPage.onCheckboxInput);
      OptionsPage.initCechboxElement('hintPanel', OptionsPage.onCheckboxInput);
      OptionsPage.initCechboxElement('textHint', OptionsPage.onCheckboxInput);
      OptionsPage.initCechboxElement('quitOnCompletion', OptionsPage.onCheckboxInput);
      OptionsPage.initCechboxElement('imageHint', OptionsPage.onCheckboxInput);
      OptionsPage.initCechboxElement('progressbarPanel', OptionsPage.onCheckboxInput);
      OptionsPage.initCechboxElement('coloredBorder', OptionsPage.onCheckboxInput);
      OptionsPage.initInputElement('progrebarPostions', OptionsPage.onNumberInput);
      OptionsPage.initCechboxElement('imageLayoutBegin', OptionsPage.onCheckboxInput);
      OptionsPage.initCechboxElement('toggleLayout', OptionsPage.onCheckboxInput);
      OptionsPage.initCechboxElement('configurationSettings', OptionsPage.onCheckboxInput);
      OptionsPage.initCechboxElement('subscriberSettings', OptionsPage.onCheckboxInput);
      OptionsPage.initCechboxElement('premiumSubscriber', OptionsPage.onCheckboxInput);
      OptionsPage.initCechboxElement('exclusiveSubscriber', OptionsPage.onCheckboxInput);
      OptionsPage.initCechboxElement('featureSettings', OptionsPage.onCheckboxInput);
      OptionsPage.initCechboxElement('initSettings', OptionsPage.onCheckboxInput);
    });
  }

  static initCechboxElement(elemId: string, eventHandler: any) {
    const divElm: HTMLElement = document.querySelector(`#${elemId}`);
    let labelElm: HTMLLabelElement = divElm.querySelector('label');
    let inputElm: HTMLInputElement = divElm.querySelector('input');
    inputElm.addEventListener('input', eventHandler);

    labelElm.innerText = OptionsPage.featuresObj[elemId][0];
    inputElm.checked = OptionsPage.featuresObj[elemId][1];
  }

  static onTextInput(event: InputEvent) {
    const currElm: HTMLInputElement = <HTMLInputElement>(<unknown>this);
    const id: string = currElm.parentElement.parentElement.id;
    OptionsPage.initObj[id] = currElm.value;
    chrome.storage.local.set({'picpass_config': OptionsPage.config });
  }

  static onNumberInput(event: InputEvent) {
    const currElm: HTMLInputElement = <HTMLInputElement>(<unknown>this);
    const id: string = currElm.parentElement.parentElement.id;
    OptionsPage.initObj[id][1] = Number(currElm.value).valueOf();
    chrome.storage.local.set({'picpass_config': OptionsPage.config });
  }

  static onCheckboxInput(event: InputEvent) {
    const currElm: HTMLInputElement = <HTMLInputElement>(<unknown>this);
    const id: string = currElm.parentElement.id;
    OptionsPage.featuresObj[id][1] = Boolean(currElm.checked).valueOf();
    chrome.storage.local.set({'picpass_config': OptionsPage.config });
  }

  static onConfigReset(event: InputEvent) {
    OptionsPage.config = require('../config.json');
    chrome.storage.local.set({ 'picpass_config': OptionsPage.config });

    // remove all keywords select rows befroe init.
    document.querySelectorAll('#keywords-list div').forEach((divElm) => divElm.remove());

    OptionsPage.initOptionsConfig();
}
}

window.addEventListener('load', function() {
  const opttionsPageObj: OptionsPage = new OptionsPage();
});
